#!/usr/bin/env node
const http = require('https');
const fs = require('fs')
const { stringify } = require("csv-stringify");
const { parse } = require('csv-parse')
const url = "https://www1.ncdc.noaa.gov/pub/data/ccd-data/"
const filesFolder = __dirname + '/files/'
const csvFolder = __dirname + '/csv/'

const get_files = () => {
    const filenames = process.argv.slice(2)
    for (let i = 0; i < filenames.length; i++) {
        const filename = filenames[i].replace(",", "");
        get_file(filename, (response) => {
            console.log(response)
        })
    }
}
const get_file = (file, cb) => {
    if (file.toLocaleLowerCase().includes(".jpg") || file.toLocaleLowerCase().includes(".pdf")) {
        return cb("File Extension not permitted.")
    }
    const fileToSave = fs.createWriteStream(filesFolder + file);
    http.get(url + file, (response) => {
        if (response.statusCode !== 200) {
            fs.unlinkSync(filesFolder + file)
            return cb('Response with status: ' + response.statusCode);
        }
        response.pipe(fileToSave)
    });

    fileToSave.on('finish', () => {
        convertToCSV(file)
        fileToSave.close()
    });
}

const convertToCSV = (file) => {
    const zip = /^[0-9]{5}/
    const dateregx = /[0-9]{6}-[0-9]{6}/
    const spaces = /\s+/g
    const columns = [
        "ZIP", "STATE", "CITY", "START DATE", "END DATE", "JAN", "FEB", "MAR",
        "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "SUM",
    ];
    const csvExt = file.split('.')[0] + ".csv"
    const stringifier = stringify({ header: true, columns: columns });
    var date = new Date(Date.now())
    var filenameFormat = date.toISOString().split('T')[0]
        .concat(`_${date.getHours().toString()
            .concat(date.getMinutes().toString())}`)
        .concat(`_${csvExt}`)
    const csvFile = fs.createWriteStream(csvFolder + filenameFormat);
    console.log(`Convertion start: ${file}`);

    fs.createReadStream(filesFolder + file)
        .pipe(parse({ delimiter: ",", from_line: 2 }))
        .on("data", function (data) {
            var row = data.toString();
            if (file.includes('.csv')) {
                var div = row.split(dateregx)
                var dates = row.match(dateregx)[0].replace("-", ",")
                row = (div[0] + dates + div[1]).replace(/\s+,/, ",")
                stringifier.write(row.split(","))
            } else {
                var div = row.split(",")
                var zipcode = row.match(zip)[0].concat(",")
                var city = div[0].split(zip)[1].concat(",")
                var rest = div[1].replace(spaces, ",").replace("-", ",")
                row = zipcode + city + rest
                stringifier.write(row.split(","))
            }
        }).on("end", () => {
            console.log(`file converted to csv`);
        })
    stringifier.pipe(csvFile);
}

get_files()

